#pragma once

#include <d3d11.h>
#include <dxgi1_3.h>
#include <directxmath.h>

class D3D11Wrapper
{
public:
	D3D11Wrapper(HWND hwnd);
	~D3D11Wrapper();

private:
	void Startup(HWND hwnd);
	void Shutdown();

	ID3D11Device* m_Device;
	ID3D11DeviceContext* m_Context;
};

