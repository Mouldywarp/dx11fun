#include "D3D11Wrapper.h"


D3D11Wrapper::D3D11Wrapper(HWND hwnd)
{
	Startup(hwnd);
}


D3D11Wrapper::~D3D11Wrapper()
{
	Shutdown();
}

void D3D11Wrapper::Startup(HWND hwnd)
{
	D3D_FEATURE_LEVEL levels[] = {
	D3D_FEATURE_LEVEL_9_1,
	D3D_FEATURE_LEVEL_9_2,
	D3D_FEATURE_LEVEL_9_3,
	D3D_FEATURE_LEVEL_10_0,
	D3D_FEATURE_LEVEL_10_1,
	D3D_FEATURE_LEVEL_11_0,
	D3D_FEATURE_LEVEL_11_1
	};

	D3D_FEATURE_LEVEL featureLevel;

	// This flag adds support for surfaces with a color-channel ordering different
	// from the API default. It is required for compatibility with Direct2D.
	UINT deviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if defined(DEBUG) || defined(_DEBUG)
	deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// Create the Direct3D 11 API device object and a corresponding context.

	HRESULT result = D3D11CreateDevice(
		nullptr,                    // Specify nullptr to use the default adapter.
		D3D_DRIVER_TYPE_HARDWARE,   // Create a device using the hardware graphics driver.
		0,                          // Should be 0 unless the driver is D3D_DRIVER_TYPE_SOFTWARE.
		deviceFlags,                // Set debug and Direct2D compatibility flags.
		levels,                     // List of feature levels this app can support.
		ARRAYSIZE(levels),          // Size of the list above.
		D3D11_SDK_VERSION,          // Always set this to D3D11_SDK_VERSION for Windows Store apps.
		&m_Device,                    // Returns the Direct3D device created.
		&featureLevel,            // Returns feature level of device created.
		&m_Context                    // Returns the device immediate context.
	);

	//if (FAILED(hr))
	//{
	//	// Handle device interface creation failure if it occurs.
	//	// For example, reduce the feature level requirement, or fail over 
	//	// to WARP rendering.
	//}

	//// Store pointers to the Direct3D 11.1 API device and immediate context.
	//device.As(&m_pd3dDevice);
	//context.As(&m_pd3dDeviceContext);


	DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(DXGI_SWAP_CHAIN_DESC));
	desc.Windowed = TRUE; // Sets the initial state of full-screen mode.
	desc.BufferCount = 2;
	desc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.SampleDesc.Count = 1;      //multisampling setting
	desc.SampleDesc.Quality = 0;    //vendor-specific flag
	desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	desc.OutputWindow = hwnd;

	IDXGIDevice3* dxgiDevice = (IDXGIDevice3*)(m_Device);
}

void D3D11Wrapper::Shutdown()
{
	if (m_Device != nullptr)
	{
		m_Device->Release();
		m_Device = nullptr;
	}

	if (m_Context != nullptr)
	{
		m_Context->Release();
		m_Context = nullptr;
	}
}
