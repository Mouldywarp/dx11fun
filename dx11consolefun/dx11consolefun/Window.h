#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <iostream>

const bool FULL_SCREEN = false;

class Window
{
public:
	Window(int width, int height);
	~Window();

	HWND GetHwnd() { return m_hwnd; }

	LRESULT CALLBACK MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);

private:
	void Startup();
	void Shutdown();

	int m_Width, m_Height;

	LPCWSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;
};

/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


/////////////
// GLOBALS //
/////////////
static Window* ApplicationHandle = 0;