#include "Shader.h"

const char* Shader::DEFAULT_ENTRY_POINT = "Main";

void Shader::BindShader(ID3D11DeviceContext* context)
{
	// Set the vertex input layout.
	context->IASetInputLayout(m_Layout);

	// Set the vertex and pixel shaders that will be used to render this triangle.
	context->VSSetShader(m_VertexShader, NULL, 0);
	context->PSSetShader(m_FragmentShader, NULL, 0);
}


void Shader::Load(
	ID3D11Device* device, 
	const WCHAR* vsFilename, 
	const WCHAR* fsFilename,
	const char* vsEntryPoint,
	const char * fsEntryPoint)
{
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* fragmentShaderBuffer;

	auto result = D3DCompileFromFile(vsFilename, NULL, NULL, vsEntryPoint, "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&vertexShaderBuffer, &errorMessage);

	if (FAILED(result))
	{
		// If the shader failed to compile it should have writen something to the error message.
		if (errorMessage)
		{
			int idf = 4;//failed to compile
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else
		{
			int dfdgdfg = 4;//no file
		}
	}

	D3DCompileFromFile(fsFilename, NULL, NULL, fsEntryPoint, "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&fragmentShaderBuffer, &errorMessage);

	// Create the vertex shader from the buffer.
	device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &m_VertexShader);
	// Create the pixel shader from the buffer.
	device->CreatePixelShader(fragmentShaderBuffer->GetBufferPointer(), fragmentShaderBuffer->GetBufferSize(), NULL, &m_FragmentShader);

	// Now setup the layout of the data that goes into the shader.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "COLOR";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	// Get a count of the elements in the layout.
	auto numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Create the vertex input layout.
	device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(),
		vertexShaderBuffer->GetBufferSize(), &m_Layout);

	vertexShaderBuffer->Release();
	fragmentShaderBuffer->Release();





	//// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	//D3D11_BUFFER_DESC matrixBufferDesc;
	//matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	//matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	//matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//matrixBufferDesc.MiscFlags = 0;
	//matrixBufferDesc.StructureByteStride = 0;

	//// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	//result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);
}


void Shader::Unload()
{
	if (m_VertexShader != nullptr) { m_VertexShader->Release(); }
	if (m_FragmentShader != nullptr) { m_FragmentShader->Release(); }
	if (m_Layout != nullptr) { m_Layout->Release(); }
	//if (m_MatrixBuffer != nullptr) { m_MatrixBuffer->Release(); }
}



//
//bool SetShaderParameters(
//	ID3D11DeviceContext* deviceContext,
//	XMMATRIX worldMatrix,
//	XMMATRIX viewMatrix,
//	XMMATRIX projectionMatrix,
//	ID3D11ShaderResourceView* texture)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	MatrixBufferType* dataPtr;
//	unsigned int bufferNumber;
//
//
//	// Transpose the matrices to prepare them for the shader.
//	worldMatrix = XMMatrixTranspose(worldMatrix);
//	viewMatrix = XMMatrixTranspose(viewMatrix);
//	projectionMatrix = XMMatrixTranspose(projectionMatrix);
//
//	// Lock the constant buffer so it can be written to.
//	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	if (FAILED(result))
//	{
//		return false;
//	}
//
//	// Get a pointer to the data in the constant buffer.
//	dataPtr = (MatrixBufferType*)mappedResource.pData;
//
//	// Copy the matrices into the constant buffer.
//	dataPtr->world = worldMatrix;
//	dataPtr->view = viewMatrix;
//	dataPtr->projection = projectionMatrix;
//
//	// Unlock the constant buffer.
//	deviceContext->Unmap(m_matrixBuffer, 0);
//
//	// Set the position of the constant buffer in the vertex shader.
//	bufferNumber = 0;
//
//	// Finanly set the constant buffer in the vertex shader with the updated values.
//	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);
//
//	// Set shader texture resource in the pixel shader.
//	deviceContext->PSSetShaderResources(0, 1, &texture);
//
//	return true;
//}
