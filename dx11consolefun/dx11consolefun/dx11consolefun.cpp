#include <d3d11.h>
#include <directxmath.h>

#include "Window.h"
#include "D3D11Wrapper.h"
#include "d3dclass.h"
#include "Camera.h"
#include "Shader.h"


using namespace DirectX;

ID3D11Buffer *m_vertexBuffer = nullptr, *m_indexBuffer = nullptr;


const WCHAR* vsFilename = L"../dx11consolefun/colour.vs";
const WCHAR* psFilename = L"../dx11consolefun/colour.fs";

struct VertexType
{
	XMFLOAT3 position;
	XMFLOAT4 color;
};

struct MatrixBufferType
{
	XMMATRIX world;
	XMMATRIX view;
	XMMATRIX projection;
};

void MakeTriangle(ID3D11Device* device)
{
	VertexType verts[3];
	unsigned long indices[3];

	// Load the vertex array with data.
	verts[0].position = XMFLOAT3(-1.0f, -1.0f, 0.0f);  // Bottom left.
	verts[0].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

	verts[1].position = XMFLOAT3(0.0f, 1.0f, 0.0f);  // Top middle.
	verts[1].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

	verts[2].position = XMFLOAT3(1.0f, -1.0f, 0.0f);  // Bottom right.
	verts[2].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

	// Load the index array with data.
	indices[0] = 0;  // Bottom left.
	indices[1] = 1;  // Top middle.
	indices[2] = 2;  // Bottom right.



	// dx11 stuff here!
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * 3;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = verts;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * 3;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
}




int main()
{
	// Make a window
	Window window(800, 600);
	//D3D11Wrapper dx11(window.GetHwnd());
	D3DClass dx11;
	dx11.Initialize(800, 600, true, window.GetHwnd(), FULL_SCREEN, 1000.0f, 0.1f);

	CameraClass camera;
	camera.SetPosition(0.0f, 0.0f, -5.0f);

	// Setup triangle
	MakeTriangle(dx11.device());

	// Compile shaders
	Shader shader(dx11.GetDevice(), vsFilename, psFilename, "ColorVertexShader", "ColorPixelShader");

	// Loop
	MSG msg;
	bool done, result;


	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user.
	done = false;
	while (!done)
	{
		// Handle the windows messages.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if (msg.message == WM_QUIT)
		{
			done = true;
		}
		else
		{
			dx11.BeginScene(0.2f, 0.6f, 1.0f, 1.0f);

			// RENDERING HERE
			unsigned int stride = sizeof(VertexType);;
			unsigned int offset = 0;
			auto deviceContext = dx11.GetDeviceContext();

			// Set the vertex buffer to active in the input assembler so it can be rendered.
			deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

			// Set the index buffer to active in the input assembler so it can be rendered.
			deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

			// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
			deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			shader.BindShader(deviceContext);

			// Render the triangle.
			deviceContext->DrawIndexed(3, 0, 0);

			dx11.EndScene();
		}
	}


	// release resources
	if (m_vertexBuffer != nullptr) { m_vertexBuffer->Release(); }
	if (m_indexBuffer != nullptr) { m_indexBuffer->Release(); }
	
	dx11.Shutdown();


	return 0;
}