#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>

class Shader
{
public:
	Shader(ID3D11Device* device, const WCHAR* vsFilename, const WCHAR* fsFilename)
	{
		Load(device, vsFilename, fsFilename, DEFAULT_ENTRY_POINT, DEFAULT_ENTRY_POINT);
	}
	Shader(ID3D11Device* device, const WCHAR* vsFilename, const WCHAR* fsFilename,
		const char* vsEntryPoint, const char * fsEntryPoint)
	{
		Load(device, vsFilename, fsFilename, vsEntryPoint, fsEntryPoint);
	}
	~Shader()
	{
		Unload();
	}

	// Bind the shader to a device context
	void BindShader(ID3D11DeviceContext* context);

private:

	ID3D11VertexShader* m_VertexShader = nullptr;
	ID3D11PixelShader* m_FragmentShader = nullptr;
	ID3D11InputLayout* m_Layout = nullptr;
	//ID3D11Buffer* m_MatrixBuffer = nullptr;


	void Load(ID3D11Device* device, const WCHAR* vsFilename, const WCHAR* fsFilename,
		const char* vsEntryPoint, const char * fsEntryPoint);

	void Unload();


	static const char* DEFAULT_ENTRY_POINT;

};


